<?php get_header(); ?>

	<header class="hero has-background background-base">
		<h1 class="align-center hide-accessible">Programs and Events</h1>

		<div class="col-md--eightcol col--centered ">
			<p>
				Through partnerships in the community and with the support of <a href="//www.nova.edu">Nova Southeastern University</a>,
				we are able to bring you art and historical exhibits, teach workshops, invite performers and speakers, provide
				nationally recognized children's programming, and more.
			</p>

			<form class="clearfix form" role="form" action="#">
				<ul>

					<li class="form__field form__field--inline">
						<label class="label" for="for">Filter</label>
						<div class="form__select">
							<select name="for">
								<option value="" <?php echo ( !get_query_var('for') ? 'selected' : ''); ?>>Everyone</option>
								<option value="public" <?php echo ( get_query_var('for') === 'public' ? 'selected' : ''); ?>>Public</option>
								<option value="academic" <?php echo ( get_query_var('for') === 'academic' ? 'selected' : ''); ?>>Academic</option>
							</select>
						</div>
					</li>

					<li class="form__field form__field--inline">
						<label class="label" for="search">Find a specific event (optional)</label>
						<input style="color: black" class="form__input form__input--prepend" placeholder="Sharkey's storytime" type="text" name="search" id="search" value="<?php echo get_query_var('search');?>">
						<button class="button button--primary--alt button--small form__input--append" type="submit" style="height: 36px;"><svg class="svg svg--search" viewBox="0 0 32 32"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use></svg> <span class="hide-accessible">Submit</span></button>
					</li>
				</ul>
			</form>

		</div>

	</header>

	<div id="content" class="has-cards hero--small">

		<main id="main" class="col-md--eightcol col--centered clearfix">
			<?php get_template_part( 'loop', 'event' ); ?>
    </main>

	</div> <!-- end #content -->

<?php get_footer(); ?>
