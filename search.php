<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class=" wrap clearfix">
				
					<header>
						<h1 class="archive-title hide-accessible">Search Results for: <?php echo esc_attr(get_search_query()); ?></h1>
						<p class="no-margin">Posts matching: <strong><?php echo esc_attr( get_search_query() ); ?></strong></p>
					</header>

				    <main id="main" class="eightcol center-grid hero clearfix">

				    	<?php get_template_part('loop', 'event'); ?>
				
    				 </main>
                
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>