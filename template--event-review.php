<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
/* ==================
 * Custom Fields
 */ $comments	=	get_post_meta( get_the_ID(), 'comments', true );
	$cost 		= 	get_post_meta( get_the_ID(), 'event_cost', true );
	$rsvp 		= 	get_post_meta( get_the_ID(), 'event_rsvp', true );
	$promo 		=	get_post_meta( get_the_ID(), 'materials_requested', true );

	if ( $promo == 'not_requested' ) {

		$promo = 'Promotional materials haven\'t been requested yet.';
		$promoButton = true;

	} elseif ( $promo == 'not_required' ) {
		
		$promo = 'No promotional materials are required for this event.';

	} elseif ( $promo == 'pending' ) {
		
		$promo = 'A requested has been submitted for promotional materials and it is currently pending.';

	} else {

		$promo == 'All promotional materials have been requested and created. Woo-hoo!';

	}

/* ==================
 * Date Options 
 */	$allday					= false;
	$multiday				= false;				
	$date_start				= get_post_meta( get_the_ID(), 'event_start', true );
	$date_options			= get_post_meta( get_the_ID(), 'scheduling_options', true );
	$start = '';
	$end = '';

	if ( !$date_options ) {
		$event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
		$event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
		$start = $date_start . $event_start_time;
		$end 	= $date_start . $event_end_time;

	} else {

		if ( in_array( "multiday", $date_options ) ) {

			$start = $date_start;					
			$multiday = true;

			if ( in_array( "allday", $date_options ) ) {

				$allday = true;
				$end = get_post_meta( get_the_ID(), 'event_end', true );						

			} else {

				$end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );
			}

		}

	}
 ?>

	<section class="align-center post-status alpha" style="background-color: #313547; color: white;">
	<?php if ( get_post_status() == 'pending' ) : ?>
		Awaiting Review
	<?php else : ?>
		Post Status Here!
	<?php endif; ?>

	</section>
	<p class="align-center epsilon">
		<?php if ( $user = get_post_meta( get_the_ID(), '_edit_last', true ) ) {
			$librarian = get_userdata( $user );
			printf(__('Last modified by <b>%1$s</b> on %2$s'), wp_specialchars( $librarian->display_name ), mysql2date(get_option('date_format'), $post->post_modified), mysql2date(get_option('time_format'), $post->post_modified));
		} ?>							
	</p>

    <div id="main" class="wrap clearfix" role="main">
	
	    <article id="post-<?php the_ID(); ?>" <?php post_class('eightcol first clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
		
		    <header class="article-header">

				<h1 class="page-title" itemprop="headline">
					<?php the_title(); ?>
				</h1>
				<p class="delta no-margin"><b>Beginning</b>: <?php echo date( 'F j, Y, g a', strtotime($start) ); ?></p>
				<p class="delta"><b>Ending</b>: <?php echo date( 'F j, Y, g a', strtotime($end) ); ?></p>
		    </header> <!-- end article header -->
	
		    <section class="post-content clearfix" itemprop="articleBody">

				<h2>Short Description</h2>
		    	<?php the_excerpt(); ?>

		    	<h2>Full Content</h2>
			    <?php the_content(); ?>

			</section> <!-- end article section -->
				
			<section class="media">
				<h2>Media</h2>

				<?php the_post_thumbnail('media-medium'); ?>

				<p>
					This is the <strong>Featured Image</strong>, the <b>16:9-ratio</b> image with which the event is 
					promoted on the web. It should have minimal or ideally no text. This must be a <b>progressive jpg</b>
					with a resolution of <b>92</b>.
				</p>

			</section>

    	</article> <!-- end article -->	 

    	<aside class="fourcol last">

    		<section class="stack-blocks">
				<h3>
					Promotional Materials
				</h3>

				<p><?php echo $promo; ?></p>

				<?php if ( $promoButton ): ?>
					<p><a class="button coral">Request Materials</a></p>
				<?php endif ?>
			</section>

			<?php $location = get_term_by( 'id', get_post_meta( get_the_ID(), 'location', true), 'location' ); ?>
	    	<h3 class="delta no-margin">Location: <?php echo $location->name; ?></h3>
	    	<p><?php echo $location->description; ?> </p>

			    	<h3 class="delta no-margin">Type of Event</h3>
			    	<p><?php echo get_the_term_list( $post->ID, 'event_type' ); ?></p>

			    	<h3 class="delta no-margin">Targeted Audiences</h3>
			    	<p><?php echo get_the_term_list( $post->ID, 'library-audience' ); ?></p>

			    	<h3 class="delta no-margin">Cost</h3>
			    	<p>
			    		<?php if ( $cost ): ?>
			    			<?php echo $cost; ?>
			    			<?php else: ?> Free
			    		<?php endif ?>
			    	</p>

			    	<h3 class="delta no-margin">Registration</h3>
			    	<p>
			    		<?php if ( $rsvp ): ?>
			    			<?php echo $rsvp; ?>
			    		<?php else: ?>
			    			None required
			    		<?php endif ?>
			    	</p>

    	</aside>
    </div><!--/#main-->				    	

	<div id="content">
	
		<div id="inner-content" class="wrap clearfix">
	
		<div class="eightcol first clearfix">
			<?php comments_template(); ?>
		</div>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->
	<?php endwhile; ?>			
<?php endif; ?>