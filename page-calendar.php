<?php get_header(); ?>

	<div id="content">
	
		<div id="inner-content" class="wrap clearfix">

			<?php// echo WP_FullCalendar::calendar($args); ?>

<?php 

/* ==================
 * $URL PARAMETERS
 */ // http://codex.wordpress.org/Function_Reference/get_query_var
add_filter( 'query_vars', 'add_calendar_query_vars' );
function add_calendar_query_vars( $vars ) {
    $vars[] = 'month';
    return $vars;
} 


function build_calendar($month,$year,$dateArray) {

	$today_date = date("d");
	$today_date = ltrim($today_date, '0');

     // Create array containing abbreviations of days of week.
     $daysOfWeek = array('S','M','T','W','T','F','S');

     // What is the first day of the month in question?
     $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

     // How many days does this month contain?
     $numberDays = date('t',$firstDayOfMonth);

     // Retrieve some information about the first day of the
     // month in question.
     $dateComponents = getdate($firstDayOfMonth);

     // What is the name of the month in question?
     $monthName = $dateComponents['month'];

     // What is the index value (0-6) of the first day of the
     // month in question.
     $dayOfWeek = $dateComponents['wday'];

     // Create the table tag opener and day headers

     $calendar = "<table class='table table--responsive table--calendar'>";
     $calendar .= "<caption>$monthName $year</caption>";
     $calendar .= "<tr>";

     // Create the calendar headers

     foreach($daysOfWeek as $day) {
          $calendar .= "<th>$day</th>";
     } 

     // Create the rest of the calendar

     // Initiate the day counter, starting with the 1st.

     $currentDay = 1;

     $calendar .= "</tr><tr>";

     // The variable $dayOfWeek is used to
     // ensure that the calendar
     // display consists of exactly 7 columns.

     if ($dayOfWeek > 0) { 
          $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>"; 
     }
     
     $month = str_pad($month, 2, "0", STR_PAD_LEFT);
  
 		while ($currentDay <= $numberDays) {

          // Seventh column (Saturday) reached. Start a new row.

          if ($dayOfWeek == 7) {

               $dayOfWeek = 0;
               $calendar .= "</tr><tr>";

          }

          $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);

          $date = "$year-$month-$currentDayRel";

	
		  	$calendar .= "<td rel='$date'><span class='table--calendar__day'>$currentDay</span>";
			
			$args = array(

 				'post_type' => 'spotlight_events',
 				'meta_key' => 'event_start', // 20141105
				'meta_query' => array(
					array(
						'key' => 'event_start',
						'value' => $year . $month . $currentDay
					),
					array(
						'key' => 'event_start_time'
					)
				)
 			);

			
			$query = new WP_Query( $args );
			remove_filter('posts_orderby','customorderby');
 
	
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
			
			/* ==================
			 *  Layout Options
			 */ $title 					= get_post_meta( get_the_ID(), 'overlay_title', true );
				$permalink 				= get_permalink();

				if ( !$title ) {
					$title = get_the_title();
				}

			/* ==================
			 * Date Options 
			 */	$allday					= false;
				$multiday				= false;				
				$date_start				= get_post_meta( get_the_ID(), 'event_start', true );
				$date_options			= get_post_meta( get_the_ID(), 'scheduling_options', true );
				$start = '';
				$end = '';

				if ( !$date_options ) {
					$event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
					$event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
					$start = $date_start . $event_start_time;
					$end 	= $date_start . $event_end_time;

				} else {

					if ( in_array( "multiday", $date_options ) ) {

						$start = $date_start;					
						$multiday = true;

						if ( in_array( "allday", $date_options ) ) {

							$allday = true;
							$end = get_post_meta( get_the_ID(), 'event_end', true );						

						} else {

							$end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );
						}

					}

				}

			$event_type = ( has_term( 'childrens', 'event_type' ) ? ' table--calendar__event--kids' : '' );

			$calendar .= '<a class="table--calendar__event' . $event_type . '" href="' . $permalink . '" title="' . $title . '"> <time class="time"><span class="table--calendar__time">' . date('g:i a', strtotime( $start ) ) . '</span></time> ' . $title . '</a>';

			endwhile;
			endif;

		  	$calendar .= "</td>"; 

          // Increment counters

          $currentDay++;
          $dayOfWeek++;

     }
     

     // Complete the row of the last week in month, if necessary

     if ($dayOfWeek != 7) { 
     
          $remainingDays = 7 - $dayOfWeek;
          $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>"; 

     }
     
     $calendar .= "</tr>";

     $calendar .= "</table>";

     return $calendar;

} ?>

<style type="text/css">

.table--calendar {
	table-layout: fixed;
}

.table--calendar tr {
	max-height: 100px;
}
.table--calendar__event {
	display: block;
	position: relative;
}
.table--calendar__event--kids:before {
	background-color: #ffae3d;
	bottom: .333em;
	content: "";
	left: -.25em;
	height: 1em;
	position: absolute;
	width: 2px;
}

.table--calendar td {
	height: 100px !important; 
	font-size: 16px;
	max-height: 150px;
	min-width: 100px;
	position: relative;
	vertical-align: top;
	overflow: hidden;
	white-space: nowrap;
}

.table--calendar time {
	display: inline;
}

.table--calendar__time {
	font-weight: bold;
}

.table--calendar__day {
	color: #ccc;
	position: absolute;
	right: .25em;
	top: 0;
}

.table--calendar__day--today {
	font-weight: bold;
}
</style>
<?php 

     $dateComponents = getdate();

     if ( isset( $_GET['month']) ) :
     	$month = $_GET['month'];
     else :
     $month = $dateComponents['mon']; 			     
 	endif;
     $year = $dateComponents['year'];

     echo build_calendar( $month, $year,$dateArray);

?>

				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>