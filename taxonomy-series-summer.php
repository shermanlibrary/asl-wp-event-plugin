<?php
/**
 *  $VARIABLES
 */
$heroBackgroundColor = '#e2624f';

if ( get_query_var( 'for' ) ) :

  $audience = get_query_var( 'for' );

  if ( $audience === 'kids' ) :
    $heroBackgroundColor = '#dd7129';

  elseif( $audience === 'teens' ) :
    $heroBackgroundColor = '#4fb456';

  else :
    $heroBackgroundColor = '#4991cc';
  endif;

endif;

?>

<?php get_header(); ?>

<div id="content" data-ng-controller="AdController as adc" data-audience="summer<?php echo ( $audience ? '-' . $audience : '' ); ?>" data-ng-cloak>

  <!-- Top of the page hero -->
  <div class="has-background" data-ng-repeat="ad in adc.ads | limitTo : 1" style="background-color: <?php echo $heroBackgroundColor; ?>;" data-ng-cloak>

    <div class="clearfix wrap">
      <div class="ad ad--hero ad--transparent card">


        <div class="col-md--sixcol ad__media align-center">
          <img src="" alt="" ng-src="{{ad.media}}">
        </div>

        <div class="col-md--sixcol">
          <div class="col-md--tencol col--centered ad__copy">
            <div class="card__header">
              <h2 class="menu__item__title">{{ ad.title }}</h2>
            </div>
            <div class="card__content">
              <p>{{ ad.excerpt }}</p>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>

  <!--Adults/Teens/Kids Navigation-->
  <?php if ( !get_query_var( 'for' ) ) : ?>
  <nav class="clearfix hero wrap" role="navigation">

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=adults" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summeradults.jpg" alt="Adult programs">
          </div>
        </div>
      </a>
    </div>

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=teens" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summerteens.jpg" alt="Teen programs">
          </div>
        </div>
      </a>
    </div>

    <div class="col-md--fourcol">
      <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/?for=kids" class="link link--undecorated">
        <div class="card no-padding">
          <div class="card__media no-margin">
            <img src="http://sherman.library.nova.edu/sites/wp-content/uploads/2017/04/summerkids.jpg" alt="Kids programs">
          </div>
        </div>
      </a>
    </div>

  </nav>
  <?php endif; ?>

  <div class="has-cards hero">

    <div class="clearfix wrap">

    <!--Sidebar-->
    <div class="col-md--fourcol">

      <nav role="navigation" class="menu menu--sidebar">
        <?php if ( get_query_var( 'for' ) ) : ?>
        <a href="https://sherman.library.nova.edu/sites/spotlight/series/summer/">Back to summer</a>
        <?php endif; ?>
        <a href="https://sherman.library.nova.edu/sites/spotlight/?searchtype=lists&search=Summer&post_type=list&sortdropdown=r" class="link link--undecorated">Book Lists</a>
        <a href="http://nsu.nova.edu/~lethesha/flyers2017.html" class="link link--undecorated">Event Flyers</a>
        <a href="http://public.library.nova.edu/card/?utm_source=pls&utm_medium=card&utm_campaign=ad-manager" class="link link--undecorated">Get a Library Card</a>
      </nav>

      <ng-repeat data-ng-repeat="ad in adc.ads" data-ng-if="$index > <?php echo ( get_query_var( 'for' ) ? '2' : '0' ); ?>" data-ng-cloak>
        <a ng-href="{{ ad.link }}" class="link link--undecorated">
          <div class="card">
            <div class="card__media">
              <img alt="ad.title" ng-src="{{ad.media}}">
            </div>
            <div class="card__header">
              <h2 class="card__title">{{ ad.title }}</h2>
            </div>
            <div class="card__content">
              <p class="zeta">{{ad.excerpt}}</p>
            </div>
          </div>
        </a>
      </ng-repeat>

    </div>

    <main class="col-md--eightcol" role="main">

      <?php if ( get_query_var( 'for' ) ) : ?>
      <div class="clearfix hero--small">
        <div class="col-md--sixcol" data-ng-repeat="ad in adc.ads | limitTo : 3" data-ng-if="$index > 0" data-ng-cloak>
          <a ng-href="{{ ad.link }}" class="link link--undecorated">
            <div class="card">
              <div class="card__media">
                <img alt="" ng-src="{{ad.media}}">
              </div>
              <div class="card__header">
                <h2 class="card__title">{{ ad.title }}</h2>
              </div>
              <div class="card__content">
                <p class="zeta">{{ad.excerpt}}</p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <?php endif; ?>

      <!-- Sign-up form -->
      <?php if ( !get_query_var( 'for' ) || get_query_var( 'for' ) === 'kids' ) :?>
      <!--Kids or Default Form-->
      <?php elseif ( get_query_var( 'for' ) === 'teens' ) : ?>
      <!--Teens form-->
      <?php elseif ( get_query_var( 'for' ) === 'adults' ) : ?>
      <!--Adult form-->
      <?php endif; ?>

      <!-- Events -->
      <?php get_template_part( 'loop', 'event-card' ); ?>

    </main>
  </div>

  </div>



</div> <!-- end #content -->
<?php get_footer(); ?>
